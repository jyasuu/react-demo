
import { Metadata } from 'next';
import Chart2 from '@/app/ui/chart2';
import Chart from '@/app/ui/chart';

export const metadata: Metadata = {
  title: 'Charts',
};


export default function Page() {
  return <><p>Charts Page</p><Chart2/></>;
}